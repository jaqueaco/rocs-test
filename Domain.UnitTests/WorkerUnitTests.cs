﻿using Xunit;

namespace Domain.UnitTests
{
    public class WorkerUnitTests
    {
        [Theory]
        [InlineData("10/10/2022 20:00:00", "10/10/2022 22:00:00")]
        [InlineData("10/10/2022 22:00:00", "10/10/2022 22:30:00")]
        [InlineData("10/10/2022 22:00:00", "10/10/2022 23:30:00")]
        [InlineData("10/10/2022 19:00:00", "10/10/2022 20:00:00")]
        public void IsInConflict_WhenOverlapPeriod_ReturnsTrueAndActivityInConflict(string startTimePeriodInConflict, string endTimePeriodInConflict)
        {
            //Arrange
            var activityTpe = ActivityType.BuildComponent;
            var startTime = "10/10/2022 21:00:00";
            var endTime = "10/10/2022 23:00:00";
            var schedule = new Schedule();
            var worker = new Worker('A', schedule);

            //Act
            var activity = schedule.AddActivity(activityTpe, DateTime.Parse(startTime), DateTime.Parse(endTime), worker,
                out _);
            var result = worker.HaveConflictWith(activityTpe.RechargeTime, DateTime.Parse(startTimePeriodInConflict),
                DateTime.Parse(endTimePeriodInConflict), out var activityInConflict);

            //Assert
            Assert.True(result);
            Assert.NotNull(activityInConflict);
            Assert.Equal(activity.Id, activityInConflict.Id);
            Assert.Equal(activity.StartTime, activityInConflict.StartTime);
            Assert.Equal(activity.EndTime, activityInConflict.EndTime);
            Assert.Equal(activity.Worker, activityInConflict.Worker);
        }

        [Theory]
        [InlineData("10/10/2022 09:00:00", "10/10/2022 10:30:00")]
        [InlineData("10/10/2022 18:00:00", "10/10/2022 19:00:00")]
        public void IsInConflict_WhenNoOverlapPeriod_ReturnsFalse(string startTimePeriodNotInConflict, string endTimePeriodNotInConflict)
        {
            //Arrange
            var activityTpe = ActivityType.BuildComponent;
            var startTime = "10/10/2022 13:00:00";
            var endTime = "10/10/2022 15:00:00";
            var schedule = new Schedule();
            var worker = new Worker('A', schedule);

            //Act
            schedule.AddActivity(activityTpe, DateTime.Parse(startTime), DateTime.Parse(endTime), worker,
                out _);
            var result = worker.HaveConflictWith(activityTpe.RechargeTime, DateTime.Parse(startTimePeriodNotInConflict),
                DateTime.Parse(endTimePeriodNotInConflict), out var activityInConflict);

            //Assert
            Assert.False(result);
            Assert.Null(activityInConflict);
        }
    }
}