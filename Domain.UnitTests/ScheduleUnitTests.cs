﻿using Moq;
using Xunit;

namespace Domain.UnitTests
{
    public class ScheduleUnitTests
    {
        [Fact]
        public void AddActivity_WhenWorkerActivitiesAreInConflict_ReturnsNullAndActivityInConflict()
        {
            //Arrange
            var startTime = DateTime.Now;
            var endTime = DateTime.Now.AddHours(3);

            var startTimeInConflict = DateTime.Now;
            var endTimeInConflict = DateTime.Now.AddHours(1);

            var schedule = new Schedule();
            var worker = new Worker('A', schedule);
            Mock<IWorker> workerMock = new();
            IActivity outActivityInConflict = new Activity(ActivityType.BuildComponent, startTimeInConflict, endTimeInConflict, worker);
            workerMock.Setup(c => c.HaveConflictWith(It.IsAny<byte>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), out outActivityInConflict))
                .Returns(true);

            //Act
            var result = schedule.AddActivity(ActivityType.BuildComponent, startTime, endTime, workerMock.Object, out var activityInConflict);

            //Assert
            Assert.Null(result);
            Assert.NotNull(activityInConflict);
            Assert.Equal(outActivityInConflict.StartTime, activityInConflict.StartTime);
            Assert.Equal(outActivityInConflict.EndTime, activityInConflict.EndTime);
        }

        [Fact]
        public void AddActivity_WhenValidInputData_ReturnActivityCreated()
        {
            //Arrange
            var startTime = DateTime.Now;
            var endTime = DateTime.Now.AddHours(3);
            var schedule = new Schedule();
            var worker = new Worker('A', schedule);
            Mock<IWorker> workerMock = new();
            var outActivityInConflict = null as IActivity;
            workerMock.Setup(c => c.HaveConflictWith(It.IsAny<byte>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), out outActivityInConflict))
                            .Returns(false);

            //Act
            var result = schedule.AddActivity(ActivityType.BuildMachine, startTime, endTime, workerMock.Object, out var activityInConflict);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(startTime, result.StartTime);
            Assert.Equal(endTime, result.EndTime);
            Assert.Equal(ActivityType.BuildMachine, result.ActivityType);
            Assert.Null(activityInConflict);
        }

        [Fact]
        public void TryUpdateActivity_WhenPeriodIsInConflict_ReturnsFalseAndActivityInConflict()
        {
            //Arrange
            var startTimeInConflict = DateTime.Now;
            var endTimeInConflict = DateTime.Now.AddHours(2);

            var schedule = new Schedule();

            Mock<IWorker> workerMock = new();
            IActivity updateActivityInConflict = new Activity(ActivityType.BuildMachine, startTimeInConflict, endTimeInConflict, workerMock.Object);
            workerMock.Setup(w => w.HaveConflictWith(It.IsAny<byte>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), out updateActivityInConflict))
                .Returns(true);

            //Act
            var result = schedule.TryUpdateActivity(updateActivityInConflict, startTimeInConflict, endTimeInConflict, out var activityInConflict);

            //Assert
            Assert.False(result);
            Assert.NotNull(activityInConflict);
            Assert.Equal(updateActivityInConflict.StartTime, activityInConflict.StartTime);
            Assert.Equal(updateActivityInConflict.EndTime, activityInConflict.EndTime);
        }

        [Fact]
        public void TryUpdateActivity_WhenInputDataIsValid_ReturnsTrue()
        {
            //Arrange
            var startTime = DateTime.Now;
            var endTime = DateTime.Now.AddHours(2);

            var updatedStartTime = DateTime.Now.AddHours(7);
            var updatedEndTime = DateTime.Now.AddHours(8);

            var schedule = new Schedule();

            Mock<IWorker> workerMock = new();
            var outActivity = null as IActivity;
            workerMock.Setup(c => c.HaveConflictWith(It.IsAny<byte>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), out outActivity))
                .Returns(false);

            //Act
            var updateActivity = schedule.AddActivity(ActivityType.BuildMachine, startTime, endTime, workerMock.Object, out outActivity);
            var result = schedule.TryUpdateActivity(updateActivity, updatedStartTime, updatedEndTime, out var activityInConflict);

            //Assert
            Assert.True(result);
            Assert.Null(activityInConflict);
        }

        [Fact]
        public void DeleteActivity_WhenActivityDoesNotExist_ActivityIsNotInScheduleActivities()
        {
            //Arrange
            var id = new Guid();
            var schedule = new Schedule();

            //Act
            schedule.DeleteActivity(id);

            //Assert
            Assert.Null(schedule.Activities.FirstOrDefault(a => a.Id == id));
        }

        [Fact]
        public void DeleteActivity_WhenActivityExist_ActivityIsDeletedFromScheduleActivities()
        {
            //Arrange
            var id = new Guid();
            var startTime = DateTime.Now.AddHours(5);
            var endTime = DateTime.Now.AddHours(6);

            var schedule = new Schedule();
            var worker = new Worker('A', schedule);
            Mock<IWorker> workerMock = new();
            var outActivityInConflict = null as IActivity;
            workerMock.Setup(c => c.HaveConflictWith(It.IsAny<byte>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), out outActivityInConflict))
                .Returns(false);

            //Act
            var activity = schedule.AddActivity(ActivityType.BuildMachine, startTime, endTime, workerMock.Object, out _);
            schedule.DeleteActivity(activity.Id);

            //Assert
            Assert.Null(schedule.Activities.FirstOrDefault(a => a.Id == id));
        }
    }
}