﻿using Xunit;

namespace Domain.UnitTests
{
    public class ActivityUnitTests
    {
        [Fact]
        public void Ctor_WhenInvalidPeriod_ThrowArgumentException()
        {
            //Arrange
            var startTime = DateTime.Now.AddHours(2);
            var endTime = DateTime.Now;
            var schedule = new Schedule();
            var worker = new Worker('A', schedule);

            //Act & Assert
            var exception = Assert.Throws<ArgumentException>(() => new Activity(ActivityType.BuildComponent, startTime, endTime, worker));
            Assert.Equal("StartTime can't be greater than EndTime", exception.Message);
        }

        [Fact]
        public void Ctor_WhenWorkerIsNull_ThrowArgumentException()
        {
            //Arrange
            var startTime = DateTime.Now.AddHours(2);
            var endTime = DateTime.Now;

            //Act & Assert
            Assert.Throws<ArgumentException>(() => new Activity(ActivityType.BuildComponent, startTime, endTime, null));
        }

        [Fact]
        public void Ctor_WhenActivityTypeIsBuildComponentAndWorkerIsAList_ThrowArgumentException()
        {
            //Arrange
            var startTime = DateTime.Now;
            var endTime = DateTime.Now.AddHours(1);
            var schedule = new Schedule();
            var worker1 = new Worker('A', schedule);
            var worker2 = new Worker('#', schedule);
            var worker3 = new List<IWorker>() { worker1, worker2 };
            var workerTeam1 = new WorkerTeam('C', schedule, worker3);

            //Act & Assert
            var exception = Assert.Throws<ArgumentException>(() => new Activity(ActivityType.BuildComponent, startTime, endTime, workerTeam1));
            Assert.Equal("Build component activity can't be performed by multiple workers", exception.Message);
        }

        [Fact]
        public void Ctor_WhenActivityTypeIsBuildComponentAndInputDataIsValid_NewBuildComponentActivityIsCreated()
        {
            //Arrange
            var startTime = DateTime.Now;
            var endTime = DateTime.Now.AddHours(3);
            var schedule = new Schedule();
            var worker = new Worker('A', schedule);

            //Act
            var result = new Activity(ActivityType.BuildComponent, startTime, endTime, worker);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(startTime, result.StartTime);
            Assert.Equal(endTime, result.EndTime);
            Assert.Equal(worker, result.Worker);
            Assert.Equal(ActivityType.BuildComponent, result.ActivityType);
        }

        [Fact]
        public void Ctor_WhenActivityTypeIsBuildMachineAndWorkerIsASingleWorker_NewBuildMachineActivityWithASingleWorkerIsCreated()
        {
            //Arrange
            var startTime = DateTime.Now;
            var endTime = DateTime.Now.AddHours(1);
            var schedule = new Schedule();
            var worker = new Worker('A', schedule);

            //Act
            var result = new Activity(ActivityType.BuildMachine, startTime, endTime, worker);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(startTime, result.StartTime);
            Assert.Equal(endTime, result.EndTime);
            Assert.Equal(worker, result.Worker);
            Assert.Equal(ActivityType.BuildMachine, result.ActivityType);
        }

        [Fact]
        public void Ctor_WhenActivityTypeIsBuildMachineAndWorkerIsAList_NewBuildMachineActivityWithWorkerTeamIsCreated()
        {
            //Arrange
            var startTime = DateTime.Now;
            var endTime = DateTime.Now.AddHours(1);
            var schedule = new Schedule();
            var worker1 = new Worker('A', schedule);
            var worker2 = new Worker('#', schedule);
            var worker3 = new List<IWorker>() { worker1, worker2 };
            var workerTeam = new WorkerTeam('C', schedule, worker3);

            //Act
            var result = new Activity(ActivityType.BuildMachine, startTime, endTime, workerTeam);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(startTime, result.StartTime);
            Assert.Equal(endTime, result.EndTime);
            Assert.Equal(workerTeam, result.Worker);
            Assert.Equal(ActivityType.BuildMachine, result.ActivityType);
        }

        [Fact]
        public void TryUpdateTimes_WhenInvalidPeriod_ThrowArgumentException()
        {
            //Arrange
            var validStartTime = DateTime.Now;
            var validEndTime = DateTime.Now.AddHours(1);

            var invalidStartTime = DateTime.Now.AddHours(2);
            var invalidEndTime = DateTime.Now;

            var schedule = new Schedule();
            var worker = new Worker('A', schedule);

            //Act & Assert
            var activity = new Activity(ActivityType.BuildComponent, validStartTime, validEndTime, worker);
            var exception = Assert.Throws<ArgumentException>(() => activity.TryUpdateTimes(invalidStartTime, invalidEndTime));
            Assert.Equal("StartTime can't be greater than EndTime", exception.Message);
        }

        [Fact]
        public void TryUpdateTimes_WhenInputDatIsValid_ReturnsTrue()
        {
            //Arrange
            var initialStartTime = DateTime.Now;
            var initialEndTime = DateTime.Now.AddHours(1);

            var newStartTime = DateTime.Now.AddHours(5);
            var newEndTime = DateTime.Now.AddHours(6);

            var schedule = new Schedule();
            var worker = new Worker('A', schedule);

            //Act
            var activity = new Activity(ActivityType.BuildComponent, initialStartTime, initialEndTime, worker);
            var result = activity.TryUpdateTimes(newStartTime, newEndTime);

            //Assert
            Assert.True(result);
        }
    }
}