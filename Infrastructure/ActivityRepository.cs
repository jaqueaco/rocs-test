﻿using System.Security.Cryptography.X509Certificates;
using Domain;
using Domain.Contracts;

namespace Infrastructure
{
    public class ActivityRepository : IActivityRepository
    {
        private readonly FactoryDbContext _factoryDbContext;

        public ActivityRepository(FactoryDbContext factoryDbContext)
        {
            _factoryDbContext = factoryDbContext ?? throw new ArgumentNullException(nameof(factoryDbContext));
        }

        public async Task<IActivity> GetByIdAsync(Guid id) => await _factoryDbContext.Activities.FindAsync(id);

        public async Task AddAsync(IActivity activity)
        {
            await _factoryDbContext.AddAsync(activity);
        }

        public void Update(IActivity activity)
        {
            _factoryDbContext.Update(activity);
            _factoryDbContext.Entry(activity).Property(x => x.ActivityType).IsModified = false;
        }

        public async Task DeleteAsync(Guid id)
        {
            var activity = await _factoryDbContext.Activities.FindAsync(id);

            if (activity == null) return;

            _factoryDbContext.Remove(activity);
        }

        public async Task SaveAsync()
        {
            await _factoryDbContext.SaveChangesAsync();
        }
    }
}