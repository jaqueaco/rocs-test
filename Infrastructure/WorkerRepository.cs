﻿using Domain;
using Domain.Contracts;

namespace Infrastructure
{
    public class WorkerRepository : IWorkerRepository
    {
        private readonly FactoryDbContext _factoryDbContext;

        public WorkerRepository(FactoryDbContext factoryDbContext)
        {
            _factoryDbContext = factoryDbContext ?? throw new ArgumentNullException(nameof(factoryDbContext));
        }

        public async Task<IWorker> GetByIdAsync(Guid id) => await _factoryDbContext.Workers.FindAsync(id);

        public async Task AddAsync(IWorker worker)
        {
            await _factoryDbContext.AddAsync(worker);
        }

        public async Task SaveAsync()
        {
            await _factoryDbContext.SaveChangesAsync();
        }
    }
}