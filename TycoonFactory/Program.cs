﻿using Application;
using Domain;
using Domain.Contracts;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

public class Program
{
    public static void Main(string[] args)
    {
        // GetConsoleArguments(string[] args,...);

        var serviceProvider = ConfigureServices();

        var factory = serviceProvider.GetService<IFactory>();

        factory.InitializeDummyData();

        Console.WriteLine($"The top 10 workers in the coming 7 days are: {string.Join(',', factory.GetTopWorkers(7, 10, DateTime.Now).Select(w => w.Id))}");
        Console.ReadLine();
    }

    private static ServiceProvider ConfigureServices()
    {
        var builder = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json")
            .AddEnvironmentVariables();

        IConfiguration config = builder.Build();

        var services = new ServiceCollection();

        services.AddDbContext<FactoryDbContext>(options => options.UseSqlServer(
               config.GetConnectionString("DbConnectionString")));

        services.AddSingleton<ISchedule, Schedule>();
        services.AddSingleton<IActivityRepository, ActivityRepository>();
        services.AddSingleton<IWorkerRepository, WorkerRepository>();
        services.AddSingleton<IFactory, Factory>();

        return services.BuildServiceProvider();
    }
}