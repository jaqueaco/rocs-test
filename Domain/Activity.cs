﻿namespace Domain
{
    public class Activity : IActivity
    {
        public Guid Id { get; }
        public DateTime StartTime { get; private set; }
        public DateTime EndTime { get; private set; }
        public IWorker Worker { get; }
        public byte RechargeTime => ActivityType.RechargeTime;
        public ActivityType ActivityType { get; }

        public Activity(ActivityType activityType, DateTime startTime, DateTime endTime, IWorker worker)
        {
            if (startTime > endTime)
            {
                throw new ArgumentException("StartTime can't be greater than EndTime");
            }

            if (activityType == ActivityType.BuildComponent && worker is not Domain.Worker)
            {
                throw new ArgumentException("Build component activity can't be performed by multiple workers");
            }

            Worker = worker ?? throw new ArgumentNullException(nameof(worker));
            Id = new Guid();
            StartTime = startTime;
            EndTime = endTime;
            ActivityType = activityType;
        }

        public bool TryUpdateTimes(DateTime startTime, DateTime endTime)
        {
            if (startTime > endTime)
            {
                throw new ArgumentException("StartTime can't be greater than EndTime");
            }

            StartTime = startTime;
            EndTime = endTime;
            return true;
        }
    }
}