﻿namespace Domain.Contracts
{
    public interface IActivityRepository
    {
        Task<IActivity> GetByIdAsync(Guid id);

        Task AddAsync(IActivity activity);

        void Update(IActivity activity);

        Task DeleteAsync(Guid id);

        Task SaveAsync();
    }
}