﻿namespace Domain.Contracts
{
    public interface IWorkerRepository
    {
        Task<IWorker> GetByIdAsync(Guid id);

        Task AddAsync(IWorker worker);

        Task SaveAsync();
    }
}