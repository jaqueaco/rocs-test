﻿namespace Domain;

public interface IWorker
{
    public char Id { get; }

    bool HaveConflictWith(byte rechargeTime, DateTime startTime, DateTime endTime, out IActivity activityInConflict);
}