﻿namespace Domain;

public class Schedule : ISchedule
{
    public List<IActivity> Activities { get; }

    public Schedule()
    {
        Activities = new List<IActivity>();
    }

    public IActivity AddActivity(ActivityType activityType, DateTime startTime, DateTime endTime, IWorker worker, out IActivity activityInConflict)
    {
        if (worker.HaveConflictWith(activityType.RechargeTime, startTime, endTime, out activityInConflict))
        {
            return null;
        }

        var activity = new Activity(activityType, startTime, endTime, worker);
        Activities.Add(activity);
        return activity;
    }

    public bool TryUpdateActivity(IActivity activity, DateTime startTime, DateTime endTime, out IActivity activityInConflict)
    {
        var isInConflict = activity.Worker.HaveConflictWith(activity.RechargeTime, startTime, endTime, out activityInConflict);
        if (isInConflict)
        {
            return false;
        }

        return activity.TryUpdateTimes(startTime, endTime);
    }

    public void DeleteActivity(Guid id)
    {
        var activity = Activities.SingleOrDefault(x => x.Id == id);
        if (activity != null)
        {
            Activities.Remove(activity);
        }
    }

    public IEnumerable<IActivity> GetActivitiesByWorker(IWorker worker)
    {
        return Activities.Where(c => c.Worker == worker);
    }
}