﻿namespace Domain;

public class WorkerTeam : IWorker
{
    public char Id { get; }

    public ISchedule Schedule { get; }

    public IEnumerable<IWorker> Workers { get; }

    public WorkerTeam(char id, ISchedule schedule, IEnumerable<IWorker> workers)
    {
        Id = id;
        Schedule = schedule;
        Workers = workers;
    }

    public bool HaveConflictWith(byte rechargeTime, DateTime startTime, DateTime endTime, out IActivity activityInConflict)
    {
        foreach (var worker in Workers)
        {
            var activities = Schedule.GetActivitiesByWorker(worker);

            foreach (var activity in activities)
            {
                if (!(endTime.AddHours(rechargeTime) < activity.StartTime || startTime > activity.EndTime.AddHours(activity.RechargeTime)))
                {
                    activityInConflict = activity;
                    return true;
                }
            }
        }
        activityInConflict = null;
        return false;
    }
}