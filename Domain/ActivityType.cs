﻿namespace Domain
{
    public class ActivityType
    {
        public byte Id { get; }
        public string Name { get; }
        public byte RechargeTime { get; }

        public static ActivityType BuildComponent = new(id: 1, name: nameof(BuildComponent), rechargeTime: 2);

        public static ActivityType BuildMachine = new(id: 2, name: nameof(BuildMachine), rechargeTime: 4);

        public ActivityType(byte id, string name, byte rechargeTime)
        {
            Id = id;
            Name = name;
            RechargeTime = rechargeTime;
        }
    }
}