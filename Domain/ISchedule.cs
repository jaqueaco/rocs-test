﻿namespace Domain;

public interface ISchedule
{
    List<IActivity> Activities { get; }

    IEnumerable<IActivity> GetActivitiesByWorker(IWorker worker);

    IActivity AddActivity(ActivityType activityType, DateTime startTime, DateTime endTime, IWorker worker, out IActivity activityInConflict);

    bool TryUpdateActivity(IActivity activity, DateTime startTime, DateTime endTime, out IActivity activityInConflict);

    void DeleteActivity(Guid id);
}