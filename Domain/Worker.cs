﻿namespace Domain
{
    public class Worker : IWorker
    {
        public char Id { get; }

        public ISchedule Schedule { get; }

        public Worker(char id, ISchedule schedule)
        {
            Id = id;
            Schedule = schedule;
        }

        public bool HaveConflictWith(byte rechargeTime, DateTime startTime, DateTime endTime, out IActivity activityInConflict)
        {
            var activities = Schedule.GetActivitiesByWorker(this);

            foreach (var activity in activities)
            {
                if (!(endTime.AddHours(rechargeTime) < activity.StartTime || startTime > activity.EndTime.AddHours(activity.RechargeTime)))
                {
                    activityInConflict = activity;
                    return true;
                }
            }

            activityInConflict = null;
            return false;
        }
    }
}