﻿namespace Domain;

public interface IActivity
{
    Guid Id { get; }
    DateTime StartTime { get; }
    DateTime EndTime { get; }
    IWorker Worker { get; }
    byte RechargeTime { get; }
    ActivityType ActivityType { get; }

    bool TryUpdateTimes(DateTime startTime, DateTime endTime);
}