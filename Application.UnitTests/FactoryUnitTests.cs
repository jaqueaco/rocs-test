﻿using Domain;
using Domain.Contracts;
using Moq;
using Xunit;

namespace Application.UnitTests
{
    public class FactoryUnitTests
    {
        [Fact]
        public void ScheduleActivity_WhenActivityIsInConflict_ReturnsNullAndActivityInConflict()
        {
            //Arrange
            var startTimeInConflict = DateTime.Now;
            var endTimeInConflict = DateTime.Now.AddHours(1);

            var schedule = new Schedule();
            Mock<IActivityRepository> activityRepositoryMock = new();
            Mock<IWorkerRepository> workerRepositoryMock = new();
            var factory = new Factory(schedule, workerRepositoryMock.Object, activityRepositoryMock.Object);

            var worker = new Worker('A', schedule);
            Mock<IWorker> workerMock = new();
            IActivity outActivityInConflict = new Activity(ActivityType.BuildComponent, startTimeInConflict, endTimeInConflict, worker);
            workerMock.Setup(c => c.HaveConflictWith(It.IsAny<byte>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), out outActivityInConflict))
                .Returns(true);

            //Act
            var result = factory.ScheduleActivity(ActivityType.BuildComponent, startTimeInConflict, endTimeInConflict, workerMock.Object, out var activityInConflict);

            //Assert
            Assert.Null(result);
            Assert.NotNull(activityInConflict);
            Assert.Equal(outActivityInConflict.StartTime, activityInConflict.StartTime);
            Assert.Equal(outActivityInConflict.EndTime, activityInConflict.EndTime);
            activityRepositoryMock.Verify(x => x.AddAsync(It.IsAny<IActivity>()), Times.Never);
            activityRepositoryMock.Verify(x => x.SaveAsync(), Times.Never);
        }

        [Fact]
        public void ScheduleActivity_WhenInputDataIsValid_ReturnsActivityScheduled()
        {
            //Arrange
            var startTime = DateTime.Now;
            var endTime = DateTime.Now.AddHours(3);
            var schedule = new Schedule();
            Mock<IActivityRepository> activityRepositoryMock = new();
            Mock<IWorkerRepository> workerRepositoryMock = new();
            var factory = new Factory(schedule, workerRepositoryMock.Object, activityRepositoryMock.Object);

            Mock<IWorker> workerMock = new();
            var outActivityInConflict = null as IActivity;
            workerMock.Setup(c => c.HaveConflictWith(It.IsAny<byte>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), out outActivityInConflict))
                .Returns(false);
            activityRepositoryMock.Setup(c => c.AddAsync(It.IsAny<IActivity>()));
            activityRepositoryMock.Setup(c => c.SaveAsync());

            //Act
            var result = factory.ScheduleActivity(ActivityType.BuildMachine, startTime, endTime, workerMock.Object, out var activityInConflict);

            //Assert
            Assert.NotNull(result);
            Assert.Null(activityInConflict);
            Assert.Equal(startTime, result.StartTime);
            Assert.Equal(endTime, result.EndTime);
        }

        [Fact]
        public void ModifyActivityTimes_WhenActivityIsInConflict_ReturnsFalseAndActivityInConflict()
        {
            //Arrange
            var startTimeInConflict = DateTime.Now;
            var endTimeInConflict = DateTime.Now.AddHours(1);

            var schedule = new Schedule();

            var worker = new Worker('A', schedule);

            Mock<ISchedule> scheduleMock = new();
            IActivity outActivityInConflict = new Activity(ActivityType.BuildMachine, startTimeInConflict, endTimeInConflict, worker);
            scheduleMock.Setup(c => c.TryUpdateActivity(It.IsAny<IActivity>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), out outActivityInConflict))
                .Returns(false);

            Mock<IActivityRepository> activityRepositoryMock = new();
            Mock<IWorkerRepository> workerRepositoryMock = new();
            var factory = new Factory(scheduleMock.Object, workerRepositoryMock.Object, activityRepositoryMock.Object);

            //Act
            var result = factory.ModifyActivityTimes(outActivityInConflict, startTimeInConflict, endTimeInConflict, out var activityInConflict);

            //Assert
            Assert.False(result);
            Assert.NotNull(activityInConflict);
            Assert.Equal(outActivityInConflict.StartTime, activityInConflict.StartTime);
            Assert.Equal(outActivityInConflict.EndTime, activityInConflict.EndTime);
            activityRepositoryMock.Verify(x => x.AddAsync(It.IsAny<IActivity>()), Times.Never);
            activityRepositoryMock.Verify(x => x.SaveAsync(), Times.Never);
        }

        [Fact]
        public void ModifyActivityTimes_WhenInputDataIsValid_ReturnsTrue()
        {
            //Arrange
            var initialStartTime = DateTime.Now;
            var initialEndTime = DateTime.Now.AddHours(1);

            var updatedStartTime = DateTime.Now.AddHours(6);
            var updatedEndTime = DateTime.Now.AddHours(7);

            var schedule = new Schedule();
            var worker = new Worker('A', schedule);

            Mock<ISchedule> scheduleMock = new();
            var outActivityInConflict = null as IActivity;

            Mock<IActivityRepository> activityRepositoryMock = new();
            Mock<IWorkerRepository> workerRepositoryMock = new();
            var factory = new Factory(scheduleMock.Object, workerRepositoryMock.Object, activityRepositoryMock.Object);

            scheduleMock.Setup(c => c.TryUpdateActivity(It.IsAny<IActivity>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), out outActivityInConflict))
                .Returns(true);
            activityRepositoryMock.Setup(c => c.Update(It.IsAny<IActivity>()));
            activityRepositoryMock.Setup(c => c.SaveAsync());

            //Act
            IActivity activity = new Activity(ActivityType.BuildComponent, initialStartTime, initialEndTime, worker);
            var result = factory.ModifyActivityTimes(activity, updatedStartTime, updatedEndTime, out var activityInConflict);

            //Assert
            Assert.True(result);
            Assert.Null(activityInConflict);
            activityRepositoryMock.Verify(x => x.Update(It.IsAny<IActivity>()), Times.Once);
            activityRepositoryMock.Verify(x => x.SaveAsync(), Times.Once);
        }
    }
}