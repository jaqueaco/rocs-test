﻿namespace Application;

public class WorkerDto
{
    public char Id { get; }

    public WorkerDto(char Id)
    {
        this.Id = Id;
    }
}