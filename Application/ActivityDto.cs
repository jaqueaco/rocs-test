﻿using Domain;

namespace Application;

public class ActivityDto
{
    public Guid Id { get; private set; }
    public DateTime StartTime { get; private set; }
    public DateTime EndTime { get; private set; }
    public string ActivityTypeName { get; private set; }
    public char WorkerId { get; private set; }

    public static ActivityDto From(IActivity activity)
    {
        return new ActivityDto()
        {
            Id = activity.Id,
            StartTime = activity.StartTime,
            EndTime = activity.EndTime,
            ActivityTypeName = activity.ActivityType.Name,
            WorkerId = activity.Worker.Id,
        };
    }
}