﻿using Domain;
using Domain.Contracts;

namespace Application
{
    public class Factory : IFactory
    {
        private readonly IWorkerRepository _workerRepository;
        private readonly IActivityRepository _activityRepository;
        private ISchedule _schedule { get; }

        public Factory(ISchedule schedule, IWorkerRepository workerRepository, IActivityRepository activityRepository)
        {
            _workerRepository = workerRepository;
            _activityRepository = activityRepository;
            _schedule = schedule;
        }

        public void InitializeDummyData()
        {
            //Initialize data
            var worker1 = new Worker('A', _schedule);
            var worker2 = new Worker('B', _schedule);
            var worker3 = new Worker('C', _schedule);
            var worker4 = new Worker('D', _schedule);
            var worker5 = new Worker('E', _schedule);
            var worker6 = new Worker('F', _schedule);
            var worker7 = new Worker('G', _schedule);
            var worker8 = new Worker('H', _schedule);
            var worker9 = new Worker('I', _schedule);
            var worker10 = new Worker('J', _schedule);

            var workerTeam1 = new WorkerTeam('K', _schedule, new List<IWorker>() { worker1, worker2 });
            var workerTeam2 = new WorkerTeam('L', _schedule, new List<IWorker>() { worker3, worker4 });
            var workerTeam3 = new WorkerTeam('M', _schedule, new List<IWorker>() { worker5, worker6, worker7 });
            var workerTeam4 = new WorkerTeam('N', _schedule, new List<IWorker>() { worker8, worker9 });

            var currentDateTime = DateTime.Now;
            _schedule.AddActivity(ActivityType.BuildComponent, currentDateTime, currentDateTime.AddHours(5), worker1, out _);
            _schedule.AddActivity(ActivityType.BuildMachine, currentDateTime.AddHours(8), currentDateTime.AddHours(10), worker2,
                out _);
            _schedule.AddActivity(ActivityType.BuildMachine, currentDateTime.AddHours(16), currentDateTime.AddHours(20), workerTeam1, out _);
            _schedule.AddActivity(ActivityType.BuildMachine, currentDateTime, currentDateTime.AddHours(2), workerTeam2, out _);
            _schedule.AddActivity(ActivityType.BuildMachine, currentDateTime.AddHours(7), currentDateTime.AddHours(12),
                workerTeam2, out _);
            _schedule.AddActivity(ActivityType.BuildMachine, currentDateTime, currentDateTime.AddHours(15), workerTeam3, out _);
            _schedule.AddActivity(ActivityType.BuildMachine, currentDateTime, currentDateTime.AddHours(2), workerTeam4, out _);
            _schedule.AddActivity(ActivityType.BuildComponent, currentDateTime, currentDateTime.AddHours(5), worker10, out _);

            //Conflicts
        }

        public ActivityDto ScheduleActivity(ActivityType activityType, DateTime startTime, DateTime endTime, IWorker worker, out IActivity activityInConflict)
        {
            try
            {
                var activity = _schedule.AddActivity(activityType, startTime, endTime, worker, out activityInConflict);

                if (activityInConflict != null)
                {
                    Console.WriteLine(
                        $"The activity couldn't be created because it is in conflict with the following activity => id: {activityInConflict.Id}, startTime: {activityInConflict.StartTime}, endTime: {activityInConflict.EndTime}");
                    return null;
                }

                _activityRepository.AddAsync(activity);
                _activityRepository.SaveAsync();
                return ActivityDto.From(activity);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                activityInConflict = null;
                return null;
            }
        }

        public bool ModifyActivityTimes(IActivity activity, DateTime startTime, DateTime endTime, out IActivity activityInConflict)
        {
            try
            {
                var isUpdated = _schedule.TryUpdateActivity(activity, startTime, endTime, out activityInConflict);
                if (activityInConflict != null)
                {
                    Console.WriteLine(
                        $"The activity couldn't be modified because it is in conflict with the following activity => id: {activityInConflict.Id}, startTime: {activityInConflict.StartTime}, endTime: {activityInConflict.EndTime}");
                    return false;
                }

                _activityRepository.Update(activity);
                _activityRepository.SaveAsync();

                return isUpdated;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                activityInConflict = null;
                return false;
            }
        }

        public async Task DeleteActivity(Guid id)
        {
            _schedule.DeleteActivity(id);
            await _activityRepository.DeleteAsync(id);
        }

        public async Task AddWorker(IWorker worker) => await _workerRepository.AddAsync(worker);

        public IEnumerable<WorkerDto> GetTopWorkers(int daysCount, int workersCount, DateTime fromDateTime)
        {
            var flattenActivities = GetFlattenActivities(_schedule.Activities);

            var activitiesForComingDays = GetActivitiesForComingDays(activities: flattenActivities, fromDateTime: fromDateTime, comingDays: daysCount);

            var workerActivitiesDurations = CalculateWorkerActivitiesDurations(activitiesForComingDays, fromDateTime);

            var topWorkers = workerActivitiesDurations.OrderByDescending(w => w.Duration).Take(workersCount).Select(wd => new WorkerDto(wd.Id));

            return topWorkers;
        }

        private static List<IActivity> GetFlattenActivities(IEnumerable<IActivity> activities)
        {
            var flattenActivities = new List<IActivity>();
            foreach (var activity in activities.Where(a => a.Worker is WorkerTeam))
            {
                foreach (var worker in (activity.Worker as WorkerTeam).Workers)
                {
                    flattenActivities.Add(new Activity(activity.ActivityType, activity.StartTime, activity.EndTime, worker));
                }
            }

            flattenActivities.AddRange(activities.Where(a => a.Worker is Worker));

            return flattenActivities;
        }

        private static IEnumerable<IActivity> GetActivitiesForComingDays(List<IActivity> activities, DateTime fromDateTime, int comingDays) => activities.Where(a => a.EndTime >= fromDateTime && a.EndTime <= fromDateTime.AddDays(comingDays));

        private static IEnumerable<(char Id, double Duration)> CalculateWorkerActivitiesDurations(IEnumerable<IActivity> activities, DateTime currentDateTime) => activities.GroupBy(a => a.Worker.Id)
            .Select(w => (w.Key, w.Sum(d => d.EndTime.Subtract(d.StartTime >= currentDateTime ? d.StartTime : currentDateTime).TotalSeconds)));
    }
}