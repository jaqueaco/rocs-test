﻿using Domain;

namespace Application;

public interface IFactory
{
    void InitializeDummyData();

    ActivityDto ScheduleActivity(ActivityType activityType, DateTime startTime, DateTime endTime,
        IWorker worker, out IActivity activityInConflict);

    bool ModifyActivityTimes(IActivity activity, DateTime startTime, DateTime endTime,
        out IActivity activityInConflict);

    Task DeleteActivity(Guid id);

    Task AddWorker(IWorker worker);

    IEnumerable<WorkerDto> GetTopWorkers(int daysCount, int workersCount, DateTime fromDateTime);
}