# Jaqueline Acosta - ROCS Hiring Test - Tycoon Factory

## General Description

The code is organized in different projects.

There are 3 main projects that represent the layers in DDD:
 * **Domain**: It contains the repository contracts and domain classes and interfaces. This is the core layer and has no dependency on any other layer.
 * **Infrastructure**: It is not fully developed, but it tries to show the access data layer using Entity Framework to access the Sql Server database. This layer depends on the Domain layer. It implements the repository interfaces (Contracts) defined in the domain (IActivityRepository, IWorkerRepository) .
 * **Application**: It contains the Factory class that expose the methods to cover the scope of the solution. It also has some Dtos to expose the information we want. In the Factory constructor the InitializeData() method is called in order to have assigned some initial workers and activities. This layer depends on the Domain and the Infrastruture layers.

On the other hand, the **DomainUnitTest** and **ApplicationUnitTests** projects contains some unit tests for testing Domain and Application classes respectively

The **TyconFactory** project is the entry point. It is a Console App that assumes we are getting some inputs from the console and based on that it calls the different methods of the Factory application class. It is not fully implemented.

---

Thanks


